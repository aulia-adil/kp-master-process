import logging
import os

import requests
import tqdm


class WorkerPostMan:
    def __init__(self, server_ip, server_port):
        self.BUFFER_SIZE = 4096
        self._server_ip = server_ip
        self._server_port = server_port

    def init_listen(self, sc):
        self._sc = sc
        self._sc.bind((self._server_ip, self._server_port))
        self._sc.listen(1)

    @staticmethod
    def send_message(message: dict, destination: str, timeout=1) -> None:
        requests.get(destination, message,
                     verify=False, timeout=timeout)

    def _processing_parameter(self, input_value):
        logging.debug(input_value)
        parameters = input_value.split("\n")[0].split()[1].split("?")[1]
        parameters = parameters.replace("%3A", ":")
        parameters = parameters.replace("%2F", "/")
        parameters = parameters.split("&")
        dict_param = {}
        for parameter in parameters:
            split_param = parameter.split("=")
            dict_param[split_param[0]] = split_param[1]
        return dict_param

    def listen_download_request(self):
        connection, address = self._sc.accept()
        return connection, address

    def send_report(self, connection, filename):
        filesize = os.path.getsize(filename)
        SEPARATOR = "~!@"
        logging.debug(f"filename: {filename}")
        logging.debug(f"filesize: {filesize}")
        connection.send(f"{filename}{SEPARATOR}{filesize}".encode())
        logging.debug(f"Successfully send: {filename}")
        progress = tqdm.tqdm(range(filesize), f"Sending {filename}",
                             unit="B", unit_scale=True, unit_divisor=1024)
        with open(filename, "rb") as f:
            while True:
                # read the bytes from the file
                bytes_read = f.read(self.BUFFER_SIZE)
                if not bytes_read:
                    # file transmitting is done
                    break
                # we use sendall to assure transimission in
                # busy networks
                connection.sendall(bytes_read)
                # update the progress bar
                progress.update(len(bytes_read))


    def listen_trigger(self):
        connection, address = self._sc.accept()
        input_value_bytes = connection.recv(self.BUFFER_SIZE)
        input_value = input_value_bytes.decode("UTF-8")

        output_value = "HTTP/1.0 200"
        output_value_bytes = output_value.encode("UTF-8")

        connection.send(output_value_bytes)
        connection.close()

        master_ip_address = address[0]
        return self._processing_parameter(input_value), master_ip_address
