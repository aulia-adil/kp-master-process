import logging
import os
import threading

import requests
import socket

import tqdm


class MasterPostMan:
    BUFFER_SIZE = 4096

    def __init__(self, server_ip: str, server_port: int) -> None:
        self.SEPARATOR = "<SEPARATOR>"
        self._server_ip = server_ip
        self._server_port = server_port

    @staticmethod
    def send_message(message: dict, destination: str, timeout=1) -> None:
        requests.get(destination, message,
                     verify=False, timeout=timeout)

    def set_server_ip(self, server_ip):
        self._server_ip = server_ip

    def set_server_port(self, server_port):
        self._server_port = server_port

    def init_listen(self, sc):
        self._sc = sc
        self._sc.bind((self._server_ip, self._server_port))
        self._sc.listen(1)

    def _socket_hander(self, connection):
        connection.recv(self.BUFFER_SIZE)

        output_value = "HTTP/1.0 200"
        output_value_bytes = output_value.encode("UTF-8")

        connection.send(output_value_bytes)
        connection.close()

    def _start_download(self, filename):
        with open(filename, "wb") as f:
            while True:
                # read 1024 bytes from the socket (receive)
                bytes_read = self._sc.recv(self.BUFFER_SIZE)
                if not bytes_read:
                    # nothing is received
                    # file transmitting is done
                    break
                # write to the file the bytes we just received
                f.write(bytes_read)

    def download(self, sc, destination, port=3245):
        logging.debug(f"destination: {destination}")
        logging.debug(f"port: {port}")
        self._sc = sc
        self._sc.settimeout(10)
        self._sc.connect((destination, port))
        self._sc.settimeout(None)
        received = self._sc.recv(self.BUFFER_SIZE).decode()
        SEPARATOR = "~!@"
        filename, filesize = received.split(SEPARATOR)
        filesize = int(filesize)

        self._start_download(filename)
        return filename


    def listen_report(self):
        connection, address = self._sc.accept()
        return connection, address

    def send_response_200(self, connection):
        connection.recv(self.BUFFER_SIZE)

        output_value = "HTTP/1.0 200"
        output_value_bytes = output_value.encode("UTF-8")

        connection.send(output_value_bytes)
        connection.close()


