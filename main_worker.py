import logging
import os
import socket

import script
from worker_process.WorkerPostMan import WorkerPostMan

DEBUG = 1

logger = logging.getLogger(__name__)

def reload_logging_config():
    from importlib import reload
    reload(logging)
    logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)

def get_my_ip_address():
    my_ip_addr = os.popen("hostname -I | cut -d' ' -f1")
    return my_ip_addr.read().strip()


def reload_logging_config_script(filename):
    from importlib import reload
    reload(logging)
    logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO,
                        filename=filename, filemode='w'
                        )


def main():
    logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
    HOST = get_my_ip_address()
    PORT = 80
    logger.info("The program is running...")
    postman = WorkerPostMan(HOST, PORT)
    logger.info(f"Worker postman configuration [HOST: {HOST}, PORT: {PORT}]")

    logger.info("Waiting for bot trigger request...")
    with socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM) as sc:
        sc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        postman.init_listen(sc)
        bot_input_parameter, master_ip_address \
            = postman.listen_trigger()
    logger.info(f"Master IP address: {master_ip_address}")
    logger.info(f"Receive input bot parameter: "
                 f"{bot_input_parameter}")

    logger.info("Execute bot...")

    filenames = f"{HOST}-report-log.txt"
    reload_logging_config_script(filenames)
    try:
        script.execute_bot(bot_input_parameter)
        reload_logging_config()
        logger.info("Bot successfully execute the script...")
    except Exception as e:
        logging.error(e)
        reload_logging_config()
        logger.warning("Bot encounter error when executing the script...")
    message = {"message": "Done"}

    postman.send_message(message, f"http://{master_ip_address}", timeout=10)

    with socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM) as sc:
        sc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        postman.init_listen(sc)
        logger.info("Listen to download request...")
        is_send = False
        while is_send == False:
            try:
                connection, address = postman.listen_download_request()
                logger.debug(f"Successfully got connection "
                              f"from master ({address})...")
                postman.send_report(connection, filenames)
                is_send = True
                connection.close()
            except Exception as e:
                print(e)

if __name__ == "__main__":
    main()