## Bromy

Steps testing in local (development) only for Linux.

These files should exist in the directory:
- `script.py`
- `inputs-for-workers.csv` 

Files that we have to modify:
- `docker-compose.yml`
- `run.sh`

The commands that we have to modify:
- `python3 main_master.py -a <amount> -c <your_csv_file>.csv`

Steps:
1. clone this repository: `git clone -b proctor-room/master-process --single-branch https://gitlab.com/aulia-adil/kp-master-process.git`
2. Change directory with this command: `cd kp-master-process/`
3. Copy `script.py` and `inputs-for-workers.csv` to the directory
4. Run `docker-compose.yml` with this command: `docker-compose up -d` and wait until the containers run
5. Increase the amount of worker container with this command: `docker-compose up -d --scale worker=<amount>`
6. Go to master container terminal with this command: `docker exec -it master /bin/bash`
7. Run these commands to to search for all available IP addresses of the worker containers. 
```shell
SERVER=$(hostname -I | cut -d' ' -f1)
nmap -sP $SERVER/20 | grep for > selenium_bot_ip_addresses.txt
```
8. If it takes to long to search, then increase `/20` in `nmap -sP $SERVER/20 | grep for > selenium_bot_ip_addresses.txt`
9. Run `python3 main_master.py -a <amount_of_worker>`. See below for the argument description
```shell
Options:
  -h, --help            show this help message and exit
  -a AMOUNT_WORKER, --amount_worker=AMOUNT_WORKER
                        Amount of worker the should be running
  -c CSV_FILE, --csv_file=CSV_FILE
                        CSV file to become parameter
```
10. Check one of the workers' status by running this command: `docker logs -f <container_name>` in your other terminal tab
11. Go back to master container terminal and type `check` to see which workers have done their job
12. When there is below sentence in the output, it means all the workers have done their job  
```
All workers have completed their task...
```
11. If you wish to proceed toward the download process, then type `exit` in the terminal. Notice that some workers might crash and therefore, above output sentence won't appear if this happens.
12. Stop the master program with `ctrl+c` after this output sentence appear
```shell
Done downloading all files...
```
13. You can copy all the output files from the container with `docker cp <container-id>:/root your/path/to/folder`

## Using Google Kubernetes Engine to carry out the test for Proctor Room

Assumptions: 
- You have created the Kubernetes service along with its nodes
- You have access `kubectl` of the Kubernetes

NOTE: 
- Please be aware that one Selenium bot uses atmost 1 vCPU and 1 GB RAM (actually I'm kind of forget about the RAM)

Steps:
1. Create a deployment of a worker
```
kubectl create deployment worker \
--image=registry.gitlab.com/aulia-adil/kp-master-process/proctor-room/worker-process \
-- python3 /root/main_worker.py
```
2. Create a deployment of a master
```
kubectl create deployment master \
--image=registry.gitlab.com/aulia-adil/kp-master-process/proctor-room/master:latest
```
4. Scale the amount of workers with this command
```
kubectl scale deployments worker --replicas <number_replicas>
```
5. Find the master container pod name with this
```
kubectl get pod -o=custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName \
 --all-namespaces | grep master
```
6. Wait until all the workers are running. You check them with this command
```
kubectl get pods
```
7. Go to main master pod. 
```
kubectl exec -it <master pod name> -- /bin/bash

# example 
kubectl exec -it master-594558f478-8hl2x -- /bin/bash
```
8. Copy `inputs-for-workers.csv` to the master
```
kubectl cp your/path/to/inputs-for-workers.csv <pod name>:/root/inputs-for-workers.csv
```
9. Follow the steps start from 7th step of the first section 


## Specific only to test Proctor Room
1. `script.py` by default is Proctor Room
2. Change `inputs-for-workers.csv` value according to what you need.
```shell
user is the username
password is the password of the user
target is the ip address of Moodle
amount is the amount of activity that the bot will do in the quiz 
```


## The Version of the Tools

| Tool | Version |
| ------ | ------ |
| Chrome       |    107.0.5304.87    |
| Selenium       |    4.5.0 (RAGU)   |
