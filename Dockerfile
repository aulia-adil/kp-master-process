FROM ubuntu:20.04

RUN apt -y update && \
    apt -y install python3.8 python3-pip nmap vim && \
    pip3 install requests==2.27.1

ENV APP_HOME /root
WORKDIR /$APP_HOME

COPY trigger_peserta_ujian.py $APP_HOME/
COPY startup.sh $APP_HOME/
COPY users_202201171702_9797.csv $APP_HOME/

CMD tail -f /dev/null