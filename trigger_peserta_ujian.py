import csv
import optparse
import os
import sys
import threading
import logging
import requests
import socket

SERVER_PORT = 80
BUFFER_SIZE = 1024
SERVER_IP = ""

logging.getLogger().setLevel(logging.DEBUG)

f_exception = open("exception.csv", "w")
csv_exception = csv.writer(f_exception)
csv_exception.writerow(['location'])
f_asynchronous = open("asynchronous.csv", "w")
csv_asynchronous = csv.writer(f_asynchronous)
csv_asynchronous.writerow(['user_id', 'activity_code'])


def processing_parameter(input_value):
    parameters = input_value.split("\n")[0].split()[1].split("?")[1]
    parameters = parameters.replace("%3A", ":")
    parameters = parameters.replace("%2F", "/")
    parameters = parameters.split("&")
    dict_param = {}
    for parameter in parameters:
        split_param = parameter.split("=")
        dict_param[split_param[0]] = split_param[1]
    return dict_param


def save_exception(params):
    csv_exception.writerow([params['location']])


def save_async(params):
    csv_asynchronous.writerow([params['user_id'], params['activity_code']])


def socket_handler(connection: socket.socket, mutex):
    mutex.acquire()
    try:
        input_value_bytes = connection.recv(BUFFER_SIZE)
        input_value = input_value_bytes.decode("UTF-8")

        output_value = "HTTP/1.0 200"
        output_value_bytes = output_value.encode("UTF-8")

        connection.send(output_value_bytes)
        connection.close()

        params = processing_parameter(input_value)
        if int(params["kode"]) == 0:
            save_exception(params)
        else:
            save_async(params)
    finally:
        mutex.release()


def listen_output():
    sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sc.bind((SERVER_IP, SERVER_PORT))
    sc.listen(1)
    mutex = threading.Lock()
    print("Waiting for input")
    while True:
        connection, address = sc.accept()

        thread = threading.Thread(target=socket_handler, args=(connection, mutex))
        thread.start()


def start_load_testing(ip_addresses, target, amount):
    TARGET = target
    AMOUNT = amount

    logging.info("Read users data")
    with open("users_202201171702_9797.csv") as data:
        data = data.readlines()
        j = 0
        i = 0
        count_success_trigger = 0

        my_ip_addr = os.popen("hostname -I | cut -d' ' -f1")
        my_ip_addr = my_ip_addr.read().strip()

        # if logging.DEBUG >= logging.root.level:
        #     my_ip_addr = f"localhost:{SERVER_PORT}"

        logging.debug(f"Master IP Address {my_ip_addr}")

        limit_ip_addr = len(ip_addresses)
        logging.debug(f"Amount of ip addresses: {limit_ip_addr}")
        while i < limit_ip_addr:
            URL_PESERTA_UJIAN = f"http://{ip_addresses[i]}/"
            datum = data[count_success_trigger].strip()
            user = datum.split(",")[0]
            params = {"user":user, "password": "examplepassword",
                      "target": TARGET, "amount": AMOUNT,
                      "masterip": my_ip_addr}
            try:
                requests.get(URL_PESERTA_UJIAN, params,
                             verify=False, timeout=1)
                count_success_trigger += 1
            except Exception as e:
                pass
                # if "SUCCESS" in str(e):
                #     count_success_trigger += 1
            i += 1
        print("Amount of peserta ujian triggered:", count_success_trigger)
        logging.info("Listening for request")
        listen_output()


def main():
    parser = optparse.OptionParser()
    parser.add_option('-t', '--target', default="http://localhost/moodle/", help='target moodle')
    parser.add_option('-a', '--amount', default="50", help='amount of activity')
    (options, args) = parser.parse_args()
    target_moodle = options.target
    amount = int(options.amount)
    logging.info("Get workers' ip address")
    raw_data = open("nmap_result.txt")
    ip_addresses = []
    for line in raw_data.readlines():
        cline = line.strip().split()
        ip_addr = cline[-1].strip("(").strip(")")
        ip_addresses.append(ip_addr)

    # if logging.DEBUG >= logging.root.level:
    #     ip_addresses = ["192.168.100.7:8999"]

    logging.info("Trigger load testing")
    try:
        start_load_testing(ip_addresses, target_moodle, amount)
    except KeyboardInterrupt as e:
        print("Interrupted")
        f_exception.close()
        f_asynchronous.close()
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


if __name__ == '__main__':
    main()