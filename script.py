import logging
import os
from random import randrange
from telnetlib import EC
from time import sleep
from time import time

import selenium
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

logger = logging.getLogger(__name__)

def use_chrome_driver():

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument('--no-sandbox')

    chrome_options.add_experimental_option('prefs', {
        'download.default_directory': os.getcwd(),
        'download.prompt_for_download': False,
    })
    logging.info('Prepared chrome options..')

    browser = webdriver.Chrome('./chromedriver',options=chrome_options)
    logging.info('Initialized chrome browser..')
    return browser


def use_firefox_driver():
    firefox_options = firefox.options.Options()
    firefox_options.set_preference('browser.download.folderList', 2)
    firefox_options.set_preference(
        'browser.download.manager.showWhenStarting', False
    )
    firefox_options.set_preference('browser.download.dir', os.getcwd())
    firefox_options.set_preference(
        'browser.helperApps.neverAsk.saveToDisk', 'text/csv'
    )

    logging.info('Prepared firefox profile..')

    driver = webdriver.Firefox(options=firefox_options)
    logging.info('Initialized firefox browser..')
    return driver

wait_duration = 60 * 1.5
separator = "!@#"

def execute_bot(parameters):
    driver = use_chrome_driver()

    location = "mengakses-homepage"
    logging.info(location)
    timeout = time() + wait_duration
    while time() < timeout:
        try:
            driver.get(parameters["target"])
            break
        except selenium.common.exceptions.WebDriverException:
            pass

    def get_element(attribute_type, attribute, location):
        try:
            return WebDriverWait(driver, wait_duration). \
                until(EC.presence_of_all_elements_located(
                (attribute_type, attribute)))[0]
        except TimeoutException:
            logging.error(f"TIMEOUT EXCEPTION{separator}KODE 0{separator}LOCATION {location}")
            raise TimeoutException()

    login_link = get_element(By.CLASS_NAME, "login", location)
    login_link.find_element(By.TAG_NAME, "a").click()

    # Isi username
    location = "mengakses-login-page"
    # if logging.DEBUG >= logging.root.level:
    #     driver.save_screenshot(f"{location}.png")
    logging.info(location)
    username_form = get_element(By.ID, "username", location)
    username_form.send_keys(parameters["user"])

    # Isi password
    password_form = get_element(By.ID, "password", location)
    password_form.send_keys(parameters["password"])

    # Submit login
    submit_login = get_element(By.ID, "loginbtn", location)
    submit_login.submit()

    # Klik tombol Site Home
    location = "mengakses-site-home"
    logging.info(location)
    # if logging.DEBUG >= logging.root.level:
    #     driver.save_screenshot(f"{location}.png")
    site_home_button = get_element(By.XPATH,
                                   "//*[@data-key='home']",
                                   location)
    site_home_button.click()

    # Klik site course-nya
    location = "mengakses-site-course"
    logging.info(location)
    site_course_button = get_element(By.CLASS_NAME,
                                     "aalink",
                                     location)
    site_course_button.click()

    # Klik quiz-nya
    location = "mengakses-quiz"
    logging.info(location)
    # if logging.DEBUG >= logging.root.level:
    #     driver.save_screenshot(f"{location}.png")
    quiz_div = get_element(By.CLASS_NAME,
                           "quiz", location)
    quiz_div.find_element(By.CLASS_NAME, "aalink").click()

    # Ambil user id untuk hitting api
    user_id = driver.find_element(By.XPATH, "//*[@data-userid]").get_attribute("data-userid")
    logging.debug(f"user_id={user_id}")

    # Klik tombol mulai atau lanjut quiz
    location = "mengakses-start-quiz-page"
    logging.info(location)
    # if logging.DEBUG >= logging.root.level:
    #     driver.save_screenshot(f"{location}.png")
    quiz_start_button = get_element(By.CLASS_NAME,
                                    "quizstartbuttondiv",
                                    location)
    quiz_start_button.find_element(By.XPATH, "//*[@method='post']").submit()

    def wait_quiz_loading():
        try:
            WebDriverWait(driver, 60 * 5).until(
                EC.visibility_of_element_located((By.ID, 'quizaccess_wifiresilience_overlay')))
            WebDriverWait(driver, 60 * 5).until(
                EC.invisibility_of_element((By.ID, 'quizaccess_wifiresilience_overlay')))
        except TimeoutException:
            raise TimeoutException("mengakses-quiz")

    # Mengunduh soal terlebih dahulu
    logging.info("memulai-quiz")

    logging.info(f"SUCCESS{separator}KODE 1{separator}"
                 f"USER_ID {user_id}{separator}ACTIVITY_CODE 5")

    wait_quiz_loading()

    def pilihan_ganda(soal_sekarang, jawaban_sebelumnya):
        radio_buttons = WebDriverWait(driver, wait_duration). \
            until(EC.presence_of_all_elements_located(
            (By.CLASS_NAME, "answer")
        ))[soal_sekarang - 1].find_elements(By.XPATH, ".//*[@type='radio']")
        jumlah_pilihan = len(radio_buttons)
        jawaban_sekarang = randrange(jumlah_pilihan)
        while jawaban_sekarang == jawaban_sebelumnya:
            jawaban_sekarang = randrange(jumlah_pilihan)
        radio_button = radio_buttons[jawaban_sekarang]
        driver.execute_script("arguments[0].click();", radio_button)
        return jawaban_sekarang

    def pindah_halaman(halaman_sebelumnya, jumlah_halaman):
        halaman_sekarang = randrange(jumlah_halaman)
        while halaman_sekarang == halaman_sebelumnya - 1:
            halaman_sekarang = randrange(jumlah_halaman)
        WebDriverWait(driver, wait_duration). \
            until(EC.visibility_of_all_elements_located(
            (By.CLASS_NAME, "thispageholder")
        )
        )[halaman_sekarang].click()
        return halaman_sekarang + 1

    def flag_soal(soal_sekarang):
        try:
            WebDriverWait(driver, wait_duration). \
                until(EC.presence_of_all_elements_located(
                (By.CLASS_NAME, "questionflag")
            ))[soal_sekarang - 1].click()
        except:
            print("ERROR")

    def clear_my_choice(soal_sekarang, jawaban_sebelumnya, message):
        if jawaban_sebelumnya != -1:
            WebDriverWait(driver, wait_duration). \
                until(EC.presence_of_all_elements_located(
                (By.CLASS_NAME, "qtype_multichoice_clearchoice")
            ))[soal_sekarang - 1].find_element(By.CLASS_NAME, "btn").click()
            jawaban_sekarang = -1
            increment = 0

            logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                         f"USER_ID {message['user_id']}{separator}"
                         f"ACTIVITY_CODE {message['activity_code']}")
            return jawaban_sekarang, increment
        else:
            increment = -1
            return jawaban_sebelumnya, increment

    def get_halaman_sekarang():
        url = driver.current_url
        n = len(url)
        i = 0
        for i in range(n - 1, -1, -1):
            if url[i] == "=":
                break
        url = url[i - n + 1].split("#")[0]
        return int(url) + 1

    def submit_quiz():
        finish_attempt = get_element(By.CLASS_NAME, "endtestlink", "mengakses-submit-quiz-button")
        finish_attempt.click()
        finish_attempt_button = get_element(By.ID, "wifi_exam_submission_finish",
                                            "mengakses-submit-quiz-button")
        finish_attempt_button.submit()

    def get_refresh_limit(amount):
        if amount <= 10:
            return 1
        elif amount <= 50:
            return 5
        elif amount <= 100:
            return 10
        elif amount <= 250:
            return 15
        elif amount <= 500:
            return 20
        elif amount <= 1000:
            return 25

    logging.debug("Count pages")

    list_halaman = WebDriverWait(driver, wait_duration). \
        until(EC.visibility_of_all_elements_located(
        (By.CLASS_NAME, "thispageholder")
    ))
    jumlah_halaman = len(list_halaman)
    list_jawaban = [-1 for i in range(jumlah_halaman)]

    i = 0
    j = 0

    halaman_sebelumnya = get_halaman_sekarang()

    PILIHAN_GANDA = 0
    PINDAH_HALAMAN = 1
    FLAG_SOAL = 2
    CLEAR_CHOICE_SOAL = 3

    start = time()
    limit = int(parameters['amount']) - 2
    refresh_limit = get_refresh_limit(limit)
    while i < limit:
        aktivitas = randrange(5)

        message = {"kode": "1", "user_id": user_id, "activity_code": aktivitas + 1}

        if aktivitas == PILIHAN_GANDA:
            # message["aktivitas"] = "PILIHAN_GANDA"
            list_jawaban[halaman_sebelumnya - 1] = pilihan_ganda(halaman_sebelumnya,
                                                                 list_jawaban[halaman_sebelumnya - 1])
            logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                         f"USER_ID {message['user_id']}{separator}"
                         f"ACTIVITY_CODE {message['activity_code']}")
        elif aktivitas == PINDAH_HALAMAN:
            # message["aktivitas"] = "PILIHAN_HALAMAN"
            halaman_sebelumnya = pindah_halaman(halaman_sebelumnya, jumlah_halaman)
            logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                         f"USER_ID {message['user_id']}{separator}"
                         f"ACTIVITY_CODE {message['activity_code']}")
        elif aktivitas == FLAG_SOAL:
            # message["aktivitas"] = "FLAG_SOAL"
            flag_soal(halaman_sebelumnya)
            logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                         f"USER_ID {message['user_id']}{separator}"
                         f"ACTIVITY_CODE {message['activity_code']}")
        elif aktivitas == CLEAR_CHOICE_SOAL:
            # message["aktivitas"] = "CLEAR_CHOICE_SOAL"
            try:
                list_jawaban[halaman_sebelumnya - 1], increment = \
                    clear_my_choice(halaman_sebelumnya,
                                    list_jawaban[halaman_sebelumnya - 1],
                                    message)
                i += increment
            except:
                i += -1
        else:
            if j > refresh_limit or i == limit - 1:
                continue
            j += 1

            # message["aktivitas"] = "OFFLINE"
            # logging.debug(message)
            # message["aktivitas"] = "ONLINE"
            logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                         f"USER_ID {message['user_id']}{separator}"
                         f"ACTIVITY_CODE {message['activity_code']}")
            logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                         f"USER_ID {message['user_id']}{separator}"
                         f"ACTIVITY_CODE {message['activity_code']}")
            sleep(1)
            driver.execute_script("ws.close()")
            sleep(1.5)

            i += 1
        logging.debug(message)

        i += 1
    end = time()
    logging.info(f"Duration: {end - start}")
    logging.info("mengakhiri-quiz")
    driver.execute_script("window.onbeforeunload = function () {return null;}")
    submit_quiz()

    message = {"kode": "1", "user_id": user_id, "activity_code": 4 + 1}
    logging.info(f"SUCCESS{separator}KODE {message['kode']}{separator}"
                 f"USER_ID {message['user_id']}{separator}"
                 f"ACTIVITY_CODE {message['activity_code']}")

    location = "berhasil-submit"
    logging.info(location)
    quiz_div = get_element(By.ID,
                           "page-mod-quiz-review", location)

    driver.quit()

if __name__ == "__main__":
    execute_bot({"user": "admin", "password": "Admin123*", "amount": 500, "target": "http://3.86.209.20/"})